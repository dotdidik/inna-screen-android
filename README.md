# hybrid-base

> A Vue.js and cordova project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# vue code config
```sh
    import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'
import axios from 'axios'

import { HTTP } from "./http-req"

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
    state: {
        sliders: []
    },
    actions: {
        loadSliders({ commit }) {
            HTTP
                .get('/show/13') //rubah dengan end point sesuai nama ruangan ex: /show/13 adalah ruangan sambisari
                .then(r => r.data)
                .then(sliders => {
                    commit('SET_SLIDERS', sliders)
                    // console.log(sliders)
                })
        }
    },
    mutations: {
        SET_SLIDERS(state, sliders) {
            state.sliders = sliders
        }
    }
});
```

# build for production with minification
npm run build

# Cordova configuration
[docs for cordova](https://cordova.apache.org/docs/en/latest/)


```