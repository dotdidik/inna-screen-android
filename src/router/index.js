import Vue from 'vue'
import Router from 'vue-router'
import appCarousel from '@/components/appCarousel'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'appCarousel',
      component: appCarousel
    }
  ]
})
