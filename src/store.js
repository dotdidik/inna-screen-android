import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'
import axios from 'axios'

import { HTTP } from "./http-req"

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
    state: {
        sliders: []
    },
    actions: {
        loadSliders({ commit }) {
            HTTP
                .get('/show/13')
                .then(r => r.data)
                .then(sliders => {
                    commit('SET_SLIDERS', sliders)
                    // console.log(sliders)
                })
        }
    },
    mutations: {
        SET_SLIDERS(state, sliders) {
            state.sliders = sliders
        }
    }
});
